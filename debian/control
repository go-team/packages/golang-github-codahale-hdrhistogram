Source: golang-github-codahale-hdrhistogram
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any
Standards-Version: 4.6.0
Homepage: https://github.com/codahale/hdrhistogram
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-codahale-hdrhistogram
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-codahale-hdrhistogram.git
XS-Go-Import-Path: github.com/codahale/hdrhistogram
Testsuite: autopkgtest-pkg-go

Package: golang-github-codahale-hdrhistogram-dev
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: implementation of Gil Tene's HDR Histogram
 This package contains a pure Go implementation of the HDR Histogram
 .
 A Histogram that supports recording and analyzing sampled data value
 counts across a configurable integer value range with configurable value
 precision within the range. Value precision is expressed as the number
 of significant digits in the value recording, and provides control over
 value quantization behavior across the value range and the subsequent
 value resolution at any given level.
